module Leaflet ( map ) where

import Prelude (Unit)
import Effect (Effect)

foreign import map:: String -> Effect Unit
