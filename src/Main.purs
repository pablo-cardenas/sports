module Main where

import Prelude (Unit, otherwise, show, ($), (*), (+), (-), (/), (<), (<>), bind, discard, (>>=))
import Leaflet as L
import Fetch (fetch)

import Data.Number (cos, sin, atan2, sqrt, asin, pow, pi)
import Data.Maybe (Maybe(..))
import Data.Int (floor, round)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_)
import Effect.Console (log)
import Data.Array (takeWhile, length, scanl, zipWith, uncons)

type Point =
  { longitude ::  Number
  , latitude :: Number
  }

radians :: Number
radians = pi/180.0

degrees :: Number
degrees = 180.0/pi

haversin :: Number -> Number
haversin x = sin (x/2.0) `pow` 2.0

geoDistance :: Point -> Point -> Number
geoDistance p0 p1 = earthRadius * atan2 (sqrt (x*x + y*y)) z
      where
      x = cosPhi1 * sinDelta
      y = cosPhi0 * sinPhi1 - sinPhi0 * cosPhi1 * cosDelta
      z = sinPhi0 * sinPhi1 + cosPhi0 * cosPhi1 * cosDelta
      p1r = {longitude: p1.longitude*radians, latitude: p1.latitude*radians}
      p0r = {longitude: p0.longitude*radians, latitude: p0.latitude*radians}
      cosPhi1 = cos p1r.latitude
      sinPhi1 = sin p1r.latitude
      cosPhi0 = cos p0r.latitude
      sinPhi0 = sin p0r.latitude
      sinDelta = sin delta
      cosDelta = cos delta
      delta = p1r.longitude - p0r.longitude


earthRadius :: Number
earthRadius = 6364.374947862788

projection_length :: Point -> Point -> Point -> Number
projection_length p x0 x1 = (l0*l0 + l*l - l1*l1)/(2.0 * l)
  where
  l0 = earthRadius * (geoDistance p x0)
  l1 = earthRadius * (geoDistance p x1)
  l = earthRadius * (geoDistance x0 x1)


seconds_to_time :: Number -> String
seconds_to_time time_seconds = (padStart2 hours) <> ":" <> (padStart2 minutes) <> ":" <> (padStart2 seconds)
  where
  hours = floor (time_seconds / 3600.0)
  minutes = (floor (time_seconds / 60.0)) - hours * 60
  seconds = round(time_seconds) - hours * 3600 - minutes * 60
  padStart2 :: Int -> String
  padStart2 x | x < 10 = "0" <> show x
              | otherwise= show x


bisect :: Array Number -> Number -> Int
bisect l x = length $ takeWhile (_ < x) l

geoInterpolate :: Point -> Point -> Number -> Point
geoInterpolate p0 p1 t = { longitude: (atan2 y x) * degrees, latitude: (atan2 z (sqrt (x * x + y * y))) * degrees }
      where
      x0 = p0.longitude * radians
      y0 = p0.latitude * radians
      x1 = p1.longitude * radians
      y1 = p1.latitude * radians
      cy0 = cos(y0)
      sy0 = sin(y0)
      cy1 = cos(y1)
      sy1 = sin(y1)
      kx0 = cy0 * cos(x0)
      ky0 = cy0 * sin(x0)
      kx1 = cy1 * cos(x1)
      ky1 = cy1 * sin(x1)
      d = 2.0 * asin(sqrt(haversin(y1 - y0) + cy0 * cy1 * haversin(x1 - x0)))
      k = sin(d)

      t1 = t*d
      bb = sin(t1) / k
      aa = sin(d - t1) / k
      x = aa * kx0 + bb * kx1
      y = aa * ky0 + bb * ky1
      z = aa * sy0 + bb * sy1

arrayToCoords :: Array Number -> Maybe Point
arrayToCoords [longitude, latitude] = Just {longitude:longitude, latitude:latitude}
arrayToCoords _ = Nothing


getCoordsDistances :: Array Point -> Maybe (Array Number)
getCoordsDistances coords = case uncons coords of 
    Just { head: x, tail: xs } -> Just $ scanl (+) 0.0 (zipWith geoDistance coords xs)
    Nothing -> Nothing



main :: Effect Unit
main = launchAff_ fetch "https://httpbin.org/get" {}
