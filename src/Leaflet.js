import * as L from "leaflet";

export const map = (domId) => () => {
  const rabbitIcon = L.icon({
    iconUrl: "/img/rabbit.svg",
    iconSize: [40, 40],
    iconAnchor: [20, 20],
  });
  const runnerIcon = L.icon({
    iconUrl: "/img/runner.svg",
    iconSize: [40, 40],
    iconAnchor: [20, 20],
  });

  const map = L.map("map", { zoomSnap: 0.5, zoomDelta: 0.5 }).setView(
    [-12.1, -77.03],
    12
  );
  const markerExpected = L.marker([51.5, -0.09], { icon: rabbitIcon }).addTo(
    map
  );
  const markerRunner = L.marker([51.5, -0.09], { icon: runnerIcon }).addTo(map);
  const circleCurrent = L.circle([51.508, -0.11]).addTo(map);
  L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
    maxZoom: 19,
    attribution: "© OpenstreetMap",
    className: "map-tiles",
  }).addTo(map);
};
