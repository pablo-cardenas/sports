import {
  arrayToCoords,
  geoDistance,
  projection_length,
  bisect,
  seconds_to_time,
  geoInterpolate,
} from "../output/Main/index.js";

import * as L from "leaflet";
import "./style.scss";

const params = new Proxy(new URLSearchParams(window.location.search), {
  get: (searchParams, prop) => searchParams.get(prop),
});

var start = params.start ? new Date(parseInt(params.start) * 1000) : new Date();
var currentLocation = { latitude: -12.1, longitude: -77.0 };
var expectedIndex = 1;
var startRecalculated = start;
var intervalTimeElapsed;
var intervalTimeRemaining;
var intervalDistanceElapsed;
var intervalDistanceRemaining;
var distanceExpected;
var remaining;

// Compute timeExpectedList, distanceExpectedList, totalTime and totalDistance
const inputTable = params.pace
  .trim()
  .split(/[\r\n]+/)
  .map((e) => e.split(/[ \t]+/).map(parseFloat));
const table =
  params.unit == "kilometer"
    ? inputTable
    : inputTable.map((e) => [e[0] / e[1], e[1], e[2]]);
const timeExpectedList = [0];
const distanceExpectedList = [0];
const cadenceList = [];
for (let i = 0; i < table.length; i++) {
  timeExpectedList.push(timeExpectedList[i] + table[i][0] * table[i][1]);
  distanceExpectedList.push(distanceExpectedList[i] + table[i][0]);
  cadenceList.push(table[i][2]);
}
const totalTime = timeExpectedList[timeExpectedList.length - 1];
const totalDistance = distanceExpectedList[distanceExpectedList.length - 1];

//////////////////
// Map creation //
//////////////////
const rabbitIcon = L.icon({
  iconUrl: "/img/rabbit.svg",
  iconSize: [40, 40],
  iconAnchor: [20, 20],
});
const runnerIcon = L.icon({
  iconUrl: "/img/runner.svg",
  iconSize: [40, 40],
  iconAnchor: [20, 20],
});

const map = L.map("map", { zoomSnap: 0.5, zoomDelta: 0.5 }).setView(
  [-12.1, -77.03],
  12
);
const markerExpected = L.marker([51.5, -0.09], { icon: rabbitIcon }).addTo(map);
const markerRunner = L.marker([51.5, -0.09], { icon: runnerIcon }).addTo(map);
const circleCurrent = L.circle([51.508, -0.11]).addTo(map);
L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution: "© OpenstreetMap",
  className: "map-tiles",
}).addTo(map);

function updatedStart() {
  const end = new Date(start.getTime() + 1000 * totalTime);
  document.getElementById("start").innerHTML = `<a href="?start=${Math.round(
    start.getTime() / 1000
  )}&pace=${encodeURIComponent(params.pace)}&unit=${params.unit}&path=${
    params.path
  }">${start.toLocaleTimeString()}</a>`;
  document.getElementById("end").innerText = end.toLocaleTimeString();
  document.getElementById("duration").innerText = seconds_to_time(totalTime);
  document.getElementById("total_distance").innerText =
    totalDistance.toFixed(3);
}

updatedStart();

/////////////////
// Geolocation //
/////////////////
navigator.geolocation.watchPosition(
  (pos) => {
    currentLocation = pos.coords;
    circleCurrent.setLatLng({
      lon: currentLocation.longitude,
      lat: currentLocation.latitude,
    });
    circleCurrent.setRadius(currentLocation.accuracy);
  },
  (error) => {},
  { enableHighAccuracy: true }
);

////////////////////
// On click focus //
////////////////////
var focus = false;
var focusIntervalId;
const focusButton = document.getElementById("focus");
focusButton.addEventListener("click", () => {
  focus = !focus;
  if (focus) {
    map.setZoom(18);
    focusIntervalId = setInterval(() => {
      map.panTo([currentLocation.latitude, currentLocation.longitude]);
    }, 1000);
    focusButton.classList.add("active");
  } else {
    clearInterval(focusIntervalId);
    focusButton.classList.remove("active");
  }
});

//////////////////////////////
// fetch GeoJSON            //
//   - setInterval          //
//   - On click recalculate //
//////////////////////////////
(async function () {
  const response = await fetch("/data/" + params.path);
  const data = await response.json();
  const coords = data.features[0].geometry.coordinates.map(arrayToCoords);

  const layer = L.geoJSON(data, {
    style: {
      color: "#0000ff",
      weight: 5,
      opacity: 0.8,
    },
  });
  layer.addTo(map);
  map.fitBounds(layer.getBounds());

  const coordsDistances = [0];
  for (let i = 1; i < coords.length; i++) {
    coordsDistances.push(
      coordsDistances[i - 1] + geoDistance(coords[i - 1], coords[i])
    );
  }
  console.log(coordsDistances[coordsDistances.length - 1]);

  //////////////////////////
  // On click recalculate //
  //////////////////////////
  document.getElementById("recalculate").addEventListener("click", () => {
    start = startRecalculated;
    updatedStart();
  });

  /////////////////
  // setInterval //
  /////////////////
  const interval = setInterval(() => {
    const currentTime = new Date();
    const elapsed = Math.min((currentTime - start) / 1000, totalTime);
    remaining = totalTime - elapsed;

    // Compute distanceExpected
    expectedIndex = Math.min(
      bisect(timeExpectedList, elapsed),
      timeExpectedList.length - 1
    );
    distanceExpected =
      ((timeExpectedList[expectedIndex] - elapsed) *
        distanceExpectedList[expectedIndex - 1] +
        (elapsed - timeExpectedList[expectedIndex - 1]) *
          distanceExpectedList[expectedIndex]) /
      (timeExpectedList[expectedIndex] - timeExpectedList[expectedIndex - 1]);

    // Compute intervalTimeElapsed and intervalTimeRemaining
    intervalTimeElapsed = elapsed - timeExpectedList[expectedIndex - 1];
    intervalTimeRemaining = timeExpectedList[expectedIndex] - elapsed;
    intervalDistanceElapsed =
      distanceExpected - distanceExpectedList[expectedIndex - 1];
    intervalDistanceRemaining =
      distanceExpectedList[expectedIndex] - distanceExpected;

    // Compute expectedLngLat
    const lap_distance = coordsDistances[coordsDistances.length - 1];
    const lap = Math.floor(distanceExpected / lap_distance);
    const lap_distance_elapsed = distanceExpected - lap * lap_distance;
    const coord_pos = bisect(coordsDistances, lap_distance_elapsed);
    const expectedLngLat = geoInterpolate(
      coords[coord_pos - 1],
      coords[coord_pos],
      (lap_distance_elapsed - coordsDistances[coord_pos - 1]) /
        (coordsDistances[coord_pos] - coordsDistances[coord_pos - 1])
    );

    // Get segment_length, projection_length_, newDirection, new_coord_pos
    let segment_length = geoDistance(coords[coord_pos - 1], coords[coord_pos]);
    let projection_length_ = projection_length(
      [currentLocation.longitude, currentLocation.latitude],
      coords[coord_pos - 1],
      coords[coord_pos]
    );
    const direction =
      projection_length_ < 0 ? -1 : projection_length_ > segment_length ? 1 : 0;
    let newDirection = direction;
    let new_coord_pos = lap * coords.length + coord_pos;
    while (direction && direction == newDirection) {
      new_coord_pos = new_coord_pos + direction;
      if (new_coord_pos < 1) {
        new_coord_pos = 1;
        newDirection = -1;
        break;
      }
      segment_length = geoDistance(
        coords[(new_coord_pos - 1) % coords.length],
        coords[new_coord_pos % coords.length]
      );
      projection_length_ = projection_length(
        [currentLocation.longitude, currentLocation.latitude],
        coords[(new_coord_pos - 1) % coords.length],
        coords[new_coord_pos % coords.length]
      );
      if (!isNaN(projection_length_)) {
        newDirection =
          projection_length_ < 0
            ? -1
            : projection_length_ > segment_length
            ? 1
            : 0;
      }
    }

    // Get remaining_length
    let remaining_length =
      newDirection == -1
        ? 0
        : newDirection == 1
        ? segment_length
        : projection_length_;

    // Get newDistance
    const newLap = Math.floor((new_coord_pos - 1) / coords.length);
    const newDistance = Math.min(
      newLap * lap_distance +
        coordsDistances[(new_coord_pos - 1) % coords.length] +
        remaining_length,
      totalDistance
    );

    // Get runnerLngLat
    const runnerLngLat = geoInterpolate(
      coords[(new_coord_pos - 1) % coords.length],
      coords[new_coord_pos % coords.length],
      remaining_length / segment_length
    );

    // Get startRecalculated
    const distance_pos = Math.min(
      bisect(distanceExpectedList, newDistance),
      distanceExpectedList.length - 1
    );
    const timeRecalculated =
      ((distanceExpectedList[distance_pos] - newDistance) *
        timeExpectedList[distance_pos - 1] +
        (newDistance - distanceExpectedList[distance_pos - 1]) *
          timeExpectedList[distance_pos]) /
      (distanceExpectedList[distance_pos] -
        distanceExpectedList[distance_pos - 1]);
    startRecalculated = new Date(
      start.getTime() + (elapsed - timeRecalculated) * 1000
    );

    // Update GUI
    document.getElementById("pace").innerText = seconds_to_time(
      table[expectedIndex - 1][1]
    );
    document.getElementById("elapsed").innerText = seconds_to_time(elapsed);
    document.getElementById("remaining").innerText = seconds_to_time(remaining);
    document.getElementById("cadence").innerText =
      cadenceList[expectedIndex - 1];
    document.getElementById("interval_elapsed").innerText =
      seconds_to_time(intervalTimeElapsed);
    document.getElementById("interval_remaining").innerText = seconds_to_time(
      intervalTimeRemaining
    );
    document.getElementById("interval_duration").innerText = seconds_to_time(
      intervalTimeElapsed + intervalTimeRemaining
    );
    document.getElementById("recalculated").innerText =
      (start > startRecalculated ? "+" : "") +
      Math.round((start - startRecalculated) / 1000);
    document.getElementById("interval_number").innerText = expectedIndex;
    document.getElementById("distance_traveled").innerText =
      distanceExpected.toFixed(3);
    document.getElementById("distance_remaining").innerText = (
      totalDistance - distanceExpected
    ).toFixed(3);
    document.getElementById("interval_distance_traveled").innerText =
      intervalDistanceElapsed.toFixed(3);
    document.getElementById("interval_distance_remaining").innerText =
      intervalDistanceRemaining.toFixed(3);
    document.getElementById("interval_total_distance").innerText = (
      intervalDistanceElapsed + intervalDistanceRemaining
    ).toFixed(3);

    markerExpected.setLatLng(expectedLngLat.slice().reverse());
    markerRunner.setLatLng(runnerLngLat.slice().reverse());
  }, 1000);
})();

///////////
// Speak //
///////////
var speak = false;
var speakIntervalId;
var speakLastExpectedIndex = 0;
var speakLastTime = start;
const speakButton = document.getElementById("speak");
speakButton.addEventListener("click", () => {
  speak = !speak;
  if (speak) {
    speakIntervalId = setInterval(() => {
      const currentTime = new Date();
      const signString = start < startRecalculated ? "menos" : "más";
      const diff = Math.abs((start - startRecalculated) / 1000);
      const diffMinutes = Math.floor(diff / 60);
      const diffSeconds = Math.round(diff - 60 * diffMinutes);
      let sentence = "";

      if (speakLastExpectedIndex != expectedIndex) {
        const cadence = table[expectedIndex - 1][2];
        const pace = table[expectedIndex - 1][1];
        const paceMinutes = Math.floor(pace / 60);
        const paceSeconds = Math.round(pace - 60 * paceMinutes);
        sentence =
          "Intervalo " +
          expectedIndex +
          ", Ritmo " +
          paceMinutes +
          " " +
          paceSeconds +
          ", Cadencia " +
          cadence;
        speakLastExpectedIndex = expectedIndex;
      } else if (currentTime - speakLastTime > 5000) {
        sentence = signString + " ";
        if (diffMinutes != 0) {
          sentence += diffMinutes + " ";
        }
        sentence += diffSeconds;
      }

      if (sentence) {
        const voice = window.speechSynthesis
          .getVoices()
          .filter((e) => /es[-_]ES/.test(e.lang))[0];
        const utterThis = new SpeechSynthesisUtterance(sentence);
        utterThis.voice = voice;
        utterThis.volume = 0.5;
        window.speechSynthesis.speak(utterThis);

        speakLastTime = currentTime;
      }
    }, 1000);
    speakButton.classList.add("active");
  } else {
    clearInterval(speakIntervalId);
    speakButton.classList.remove("active");
  }
});

///////////////
// Metronome //
///////////////
var metronome = false;
var metronomeIntervalId;
var audioCtx;

const metronomeButton = document.getElementById("metronome");
metronomeButton.addEventListener("click", () => {
  metronome = !metronome;
  if (metronome) {
    audioCtx = new AudioContext();
    let lastNote = audioCtx.currentTime;
    metronomeIntervalId = setInterval(() => {
      const step = 60 / cadenceList[expectedIndex - 1];
      const diff = audioCtx.currentTime - lastNote;
      if (diff >= step / 2) {
        const nextNote = lastNote + step;

        // Create an Oscillator and a Gain node
        const oscillator = new OscillatorNode(audioCtx, {
          type: "sine",
          //frequency: 349.2282,
          frequency: 440.0,
          //frequency: 554.3653,
        });
        const gainNode = new GainNode(audioCtx, { gain: 0.4 });

        // Connect both nodes to the speakers
        oscillator.connect(gainNode);
        gainNode.connect(audioCtx.destination);

        // Now that everything is connected, starts the sound
        oscillator.start(nextNote);
        oscillator.stop(nextNote + step / 2);
        gainNode.gain.setValueAtTime(gainNode.gain.value, nextNote + step / 4);
        gainNode.gain.exponentialRampToValueAtTime(0.0001, nextNote + step / 2);
        lastNote = nextNote;
      }
    }, (60 * 1000) / 180 / 4);
    metronomeButton.classList.add("active");
  } else {
    clearInterval(metronomeIntervalId);
    audioCtx.close();
    metronomeButton.classList.remove("active");
  }
});

////////////
// Notify //
////////////
var notify = false;
var notifyIntervalId;
var notifyLastExpectedIndex = 0;
var notifyLastTime = start;
const notifyButton = document.getElementById("notify");
notifyButton.addEventListener("click", () => {
  notify = !notify;
  if (notify) {
    notifyIntervalId = setInterval(() => {
      const currentTime = new Date();

      if (
        notifyLastExpectedIndex != expectedIndex ||
        currentTime - notifyLastTime > 60 * 1000
      ) {
        const cadence = table[expectedIndex - 1][2];
        const pace = table[expectedIndex - 1][1];
        const paceMinutes = Math.floor(pace / 60);
        const paceSeconds = Math.round(pace - 60 * paceMinutes);
        const body =
          `\nInterval ${expectedIndex}:` +
          `\n${seconds_to_time(intervalTimeRemaining)}` +
          `\n${intervalDistanceRemaining.toFixed(3)}km` +
          `\nTotal:` +
          `\n${seconds_to_time(remaining)}` +
          `\n${(totalDistance - distanceExpected).toFixed(3)}km`;

        Notification.requestPermission((result) => {
          if (result === "granted") {
            navigator.serviceWorker.ready.then((registration) => {
              registration.showNotification("Pacer", {
                body: body,
                tag: 1,
              });
            });
          }
        });
        notifyLastExpectedIndex = expectedIndex;
        notifyLastTime = currentTime;
      }
    }, 1000);
    notifyButton.classList.add("active");
  } else {
    clearInterval(notifyIntervalId);
    notifyButton.classList.remove("active");
  }
});

//////////////
// WakeLock //
//////////////
// The wake lock sentinel.
let wakeLock = null;

// Function that attempts to request a wake lock.
(async function () {
  wakeLock = await navigator.wakeLock.request("screen");
})();

document.addEventListener("visibilitychange", async () => {
  if (wakeLock !== null && document.visibilityState === "visible") {
    wakeLock = await navigator.wakeLock.request("screen");
  }
});

///////////////////
// ServiceWorker //
///////////////////

//window.onload = () => {
//  "use strict";
//  if ("serviceWorker" in navigator && document.URL.split(":")[0] !== "file") {
//    navigator.serviceWorker.register("sw.js");
//  }
//};
navigator.serviceWorker.register("sw.js");
